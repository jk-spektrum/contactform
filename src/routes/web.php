<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ContactformController;

Route::get('/mail', [MailController::class, 'sendMail']);

Route::get('/',[ContactformController::class, 'index']);
Route::resource('contactform', ContactformController::class)->only('index','show','create');
