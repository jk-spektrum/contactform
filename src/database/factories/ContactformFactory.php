<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Contactform>
 */
class ContactformFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'firstname' => fake()->firstName(),
            'lastname' => fake()->lastName(),
            'street' => fake()->streetName(),
            'housenumber' => fake()->numberBetween(1,99),
            'zip' => fake()->numberBetween(10000,99999),
            'city' => fake()->city(),
            'email' => fake()->safeEmail()
        ];
    }
}
