<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('contactforms', function (Blueprint $table) {
            $table->string('firstname');
            $table->string('lastname');
            $table->string('street')->nullable();
            $table->string('housenumber')->nullable();
            $table->unsignedInteger('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('email');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropColumns('id','firstname','lastname','street','housenumber','zip','city','email');
    }
};
