<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactdataStored;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{


    public function sendMail(){
        Mail::to('customer@fake.com')->send(new ContactdataStored);


        return view('emails.data_created');
    }


}
