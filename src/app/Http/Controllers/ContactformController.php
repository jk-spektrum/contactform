<?php

namespace App\Http\Controllers;

use App\Models\Contactform;
use Illuminate\Http\Request;
use App\Http\Controllers\MailController;

class ContactformController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return @inertia(
            'Contactform/Index',
            [
                'contactforms' => Contactform::all()
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Contactform::create($request->all());

        return redirect('mail');

    }

    /**
     * Display the specified resource.
     */
    public function show(Contactform $contactform)
    {
        return @inertia(
            'Contactform/Show',
            [
                'contactform' => $contactform
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
