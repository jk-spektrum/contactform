# Install and Set Up Laravel with Docker Compose

Setting up Laravel in the local environment with Docker using Nginx, Postgresql, PHP and mailhog.


## How to Install and Run the Project

1. ```git clone git@github.com:hanieas/Docker-Laravel.git```
2. ```cd src```
3. ```composer install```
4. ```npm install```
5. ```docker-compose build && docker compose up -d```
6. new terminal ```cd scr``` ```npm run dev```
7. new terminal ```cd scr``` ```php artisan serve```
8. You can see the project on ```127.0.0.1:8080```
9. You can enter Mailhog webmailer on ```0.0.0.0:8025```


## PostgreSQL

1. docker will create local volume ```postgres-data```
